var Post = require('../../models/post')
var express = require('express')
var router = express.Router()
var websokets = require('../../websokets')
//var pubsub = require('../../pubsub')

router.get('/',function(req, res, next){
	//console.log("posts.js:get connected")
	Post.find()
	.sort('-date')
	.exec(function(err, posts){
		if(err) { return next(err) }
		res.json(posts)
	})
})
router.post('/',function(req, res, next){
	//console.log("posts.js:post connected")
	var post = new Post({body:req.body.body})
	post.username = req.auth.username
	post.save(function(err, post){
		if(err) { return next(err) }
		websokets.broadcast('new_post', post)
		//pubsub.publish('new_post',post)
		res.json(201,post)
	})
})

/*pubsub.subscribe('new_post', function(post){
	websokets.broadcast('new_post',post)
})*/

module.exports = router