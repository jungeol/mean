var gulp = require('gulp')
var concat = require('gulp-concat')
var sourcemaps = require('gulp-sourcemaps')
var uglify = require('gulp-uglify')
var ngAnnotate = require('gulp-ng-annotate')


gulp.task('watch:js',['js'],function(){
	gulp.watch('ng/**/*.js',['js'])
})

gulp.task('js', function(){
	gulp.src(['ng/module.js','ng/**/*.js'])
	.pipe(sourcemaps.init())
	.pipe(concat('app.js'))
	.pipe(ngAnnotate())
	.pipe(uglify())
	.pipe(sourcemaps.write())
	.pipe(gulp.dest('assets'))
})

//gulp 아래에 존재하는 모든 파일을 읽음
var fs = require('fs')
fs.readdirSync(__dirname + '/gulp').forEach(function(task){
	require('./gulp/' + task)
})

//node, watch:js, watch:css 작업 통합
gulp.task('dev',['watch:css','watch:js','dev:server'])
