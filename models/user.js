var db = require('../db')
var user = db.Schema({
	username:{type:String, required:true},
	password:{type:String, required:true, select:false}	//클라이언트에 비밀번호 전송X
})
module.exports = db.model('User', user)