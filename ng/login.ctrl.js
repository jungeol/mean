angular.module('app')
.controller('LoginCtrl', function($scope, UserSvc,$window){
	$scope.login = function(username, password){
		return UserSvc.login(username,password)
		.then(function(response){
			//console.log(response)
			$scope.$emit('login', response.data)
			$window.location.href= "#";	//redirecting ng-view
		},function(response){
			console.log(response)
			$window.alert("아이디와 비밀번호를 확인해주세요")
		})
	}
})