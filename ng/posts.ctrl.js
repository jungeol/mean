angular.module('app')
.controller('PostsCtrl', function($scope, PostsSvc){
	$scope.addPost = function(){
		if($scope.postBody && $scope.currentUser){
			PostsSvc.create({
				username:$scope.currentUser.username,
				body:$scope.postBody
			}).success(function(post){
				//$scope.posts.unshift(post) //밑에 구현이 되어있으므로 제거
				$scope.postBody = null
			})
		}
		else{
			console.log("'not logined' or 'no words'")
		}
	}

	//웹소켓으로 new_post가 도착하면 업데이트
	$scope.$on('ws:new_post', function(_,post){
		$scope.$apply(function(){
			$scope.posts.unshift(post)
		})
	})

	PostsSvc.fetch().success(function(posts){
		$scope.posts = posts
	})
})