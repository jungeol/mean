angular.module('app')
.service('WebSocketSvc',function($rootScope, $window){
	function webSoketHost(){
		if($window.location.protocal == 'https:'){
			return 'wss://'+window.location.host
		}
		else{
			return 'ws://'+window.location.host
		}
	}

	var connection
	this.connect = function(){
		//var url = 'ws://localhost:3000'
		console.log(webSoketHost())
		connection = new WebSocket(webSoketHost())
		connection.onmessage = function(e){
			var payload = JSON.parse(e.data)
			$rootScope.$broadcast('ws:'+payload.topic, payload.data)
		}
	}
	this.send = function(topic, data){
		var json = JSON.stringify({topic:topic, data:data})
		connection.send(json)
	}
	/*(function connect(){
		var url = 'ws://localhost:3000'
		var connection = new WebSocket(url)
		connection.onclose = function(e){
			console.log('WebSocket closed. Reconnecting...')
			$timeout(connect,10*1000)
		}
		connection.onopen = function(){
			console.log('WebSoket connection')
		}
		connection.onmessage = function(e){
			console.log(e)
			var payload = JSON.parse(e.data)
			$rootScope.$broadcast('ws:'+payload.topic, payload.data)
		}
	})*/
}).run(function (WebSocketSvc){
	WebSocketSvc.connect()
})