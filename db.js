var mongoose = require('mongoose');

var dbUrl = 'mongodb://localhost/social'
//protractor 전용 db
//if(process.env.PORT) { dbUrl = dbUrl+'test' }

mongoose.connect(dbUrl, function(){
	console.log('mongoDB connected');
});
module.exports = mongoose;