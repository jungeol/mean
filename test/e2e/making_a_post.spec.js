var db = require('../../db')
var chai = require('chai')
chai.use(require('chai-as-promised'))
var expect = chai.expect

describe('making a post', function(){
	it('logs in and creates a new post', function(){
		//홈페이지로 간다
		browser.get('http://localhost:3001')

		//'login'을 누른다
		element(by.css('nav .login')).click() 

		//로그인 폼을 채우고 제출한다
		element(by.model('username')).sendKeys('dickeyxxx')
		element(by.model('password')).sendKeys('pass')
		element(by.css('form .btn')).click()

		//글쓰기 페이지에서 새로운 글을 작성해 제출한다
		var post = 'my new post'+Math.random()
		element(by.model('postBody')).sendKeys(post)
		element(by.css('form .btn')).click()

		//사용자는 페이지에 처음으로 올라온 글을 볼 수 있어야한다
		/*element.all(by.css('ul.list-group li')).first().getText().then(function(text){
			expect(text).to.contain(text)
		}) //미들웨어로 해당코드를 밑의 코드로 간략화*/
		expect(element.all(by.css('ul.list-group li')).first().getText()).to.eventually.contain(post)

		//브라우저 멈추기
		//browser.pause()	//not working on windows - it's bug
	})
	afterEach(function(){
		//db 지우기
		//db.connection.db.dropDatabase() 
	})
})