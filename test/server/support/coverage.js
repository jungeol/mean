var path = require('path')
var blanket = require('blanket')

var filename = path.resolve(__dirname, '../../../controllers')
blanket({
	pattern:[
		filename
	]
})